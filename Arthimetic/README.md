Introduction:
-------------

In this exercise, we show that performing float (double) divisions are much faster than doing integer (long) divisions.

Descriptions:
-------------

For improving performance, employ float divisions as supposed to integer divisions only if you meet the following condition:

a) The range of your operations within your application must be within the float range (+/- 3.40282e+38).

Why float division is faster than integer division? 

In float, only mantissa division has to be performed while exponent part requires subtraction.

Results:
--------

Attached code was tested in ubuntu 20.04 and gcc 9.3.0 (c++17 with o3 optimization). 

For quick speedup numbers: https://quick-bench.com/q/irFBs_-fsps5B6hWda1011nBVlY



