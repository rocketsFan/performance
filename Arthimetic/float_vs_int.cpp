#include <iostream>
#include <chrono>
#include <stdlib.h>
#include <time.h>

using namespace std;

#define NUM_ITERATION 10000000

int main()
{
    //volatile is important to prevent compiler optimization
    //without volatile, O3 optimization kicks in and both perform the same; something to be investigated 
	volatile int iop1, iop2, iresult;
    volatile float fop1, fop2, fresult;
    volatile int iresultfromF;

    srand(time(0));

    iop1 = rand() % 100000;
    iop2 = rand() % 1000;

    fop1 = iop1;
    fop2 = iop2;

    std::cout<<"iop1: "<<iop1<<" iop2: "<<iop2<<" fop1: "<<fop1<<" fop2: "<<fop2<<std::endl;

    auto start = chrono::steady_clock::now();

    for(int i = 0; i < NUM_ITERATION; ++i)
    {
        iresultfromF = fop1 / fop2;
    }

    auto end = chrono::steady_clock::now();
    cout << "Elapsed time in microseconds for float division : "
            << chrono::duration_cast<chrono::microseconds>(end - start).count()
                    << " µs" << endl;
    start = chrono::steady_clock::now();

    for(int i = 0; i < NUM_ITERATION; ++i)
    {
        iresult = iop1 / iop2;
    }

    end = chrono::steady_clock::now();
    cout << "Elapsed time in microseconds integer point division : "
            << chrono::duration_cast<chrono::microseconds>(end - start).count()
                    << " µs" << endl;

    std::cout<<" Integer result: "<<iresult<<" Float result: "<<iresultfromF<<std::endl;
	return 0;
}

